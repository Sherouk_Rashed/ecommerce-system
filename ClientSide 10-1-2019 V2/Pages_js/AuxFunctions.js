//...........ALL Global Variables
var head = document.getElementById("myHead");
var body = document.getElementById("bodyId");
var chosenItemId;
var products = 0;
/* 
.
. Style/Load Page functions are responsible for rendering each page script and style
.
*/
function loadPage(url) {
    
    if (document.getElementById("myScript")) 
    {
        var oldScript = document.getElementById("myScript");
            body.removeChild(oldScript);  
    }
    var newScript = document.createElement("script");
        newScript.setAttribute("id","myScript");
        newScript.setAttribute("type","text/javascript");
        newScript.setAttribute("src",url);
        body.appendChild(newScript);
    
}

function stylePage(url) {
    
    
    if (document.getElementById("myLink")) 
    {
        var oldStyle = document.getElementById("myLink");
            head.removeChild(oldStyle);
    }
    var newStyle = document.createElement("link");
        newStyle.setAttribute("id","myLink");
        newStyle.setAttribute("rel","stylesheet");
        newStyle.setAttribute("href",url);
        head.appendChild(newStyle);
}
/* 
.
. clearPageContent functions are responsible for rendering each page content
.
*/
function clearPageContent() {
    if(document.getElementById("mainDiv")) {
        var oldMainDiv = document.getElementById("mainDiv");
            body.removeChild(oldMainDiv);

    }
    

    if(document.getElementById("headerClass")) {
        var oldHeader  = document.getElementById("headerClass");
            body.removeChild(oldHeader);

    }
    if(document.getElementById("chOut")) {
        var oldChOut = document.getElementById("chOut");
            body.removeChild(oldChOut);  
        
    }
}
/* 
.
. Create Header function are responsible for rendering The Header at each page
.
*/
function createHeader() {
    
    var header = document.createElement("header");
        header.setAttribute("class","headerClass");
        header.setAttribute("id","headerClass");

    var labelHome = document.createElement("a");
        labelHome.setAttribute("href","#");
        labelHome.innerHTML = "Home"; 
        labelHome.addEventListener("click",function()
        {  
            clearPageContent();
            stylePage("Style_css/homePage.css");
            loadPage("Pages_js/homePage.js");

        }); 

    
    var labelAbout = document.createElement("a"); 
        labelAbout.setAttribute("href","#");
        labelAbout.innerHTML = "About";
        labelAbout.addEventListener("click",function()
        {  
            clearPageContent();
            stylePage("Style_css/aboutPage.css");
            loadPage("Pages_js/aboutPage.js");

        }); 

    var labelContactUs = document.createElement("a");
        labelContactUs.setAttribute("href","#");
        labelContactUs.innerHTML = "Contact Us";
        labelContactUs.addEventListener("click",function()
        {  
            clearPageContent();
            stylePage("Style_css/contactUs.css");
            loadPage("Pages_js/contactUs.js");

        }); 

        header.appendChild(labelHome);
        header.appendChild(labelAbout);
        header.appendChild(labelContactUs);
        body.appendChild(header);
}
/* 
.
. Function update Cart -> takes object of cart from the local storage and add to it new item/product
.
*/
function updateCart(itemId) {
var allCartInfo;   
var myCartCollection ;
var totalPrice;
var totalQty;
var itrInProducts = 0;
var myflag = "new";
var itemOrder ;   
var qty = 0 ;  


for (var i = 0; i < products.length; i++)
        {
            Object.entries(products[i]).forEach(function([key,value]) { 
                if(key == "ProductId" && value == itemId)
                {       
                    itrInProducts = i;   
                }

            });
        }

//console.log(itrInProducts);
if( JSON.parse(localStorage.getItem("allCartInfo")) )                            
    {   
        allCartInfo = JSON.parse(localStorage.getItem("allCartInfo"));
        myCartCollection = allCartInfo["myCartCollection"];
        totalPrice = allCartInfo["totalPrice"];
        totalQty = allCartInfo["totalQty"];

        for (var itr in myCartCollection)
        {

            Object.entries(myCartCollection[itr]).forEach(function([key,value]) {  
                
                if(key == itemId && isAccepted(itemId))         //....Item already in cart Thus increase Qty
                {
                    itemOrder = itr;   
                    qty = value + 1;  
                    myflag = "updated";
                }else if(   key == itemId && (!(isAccepted(itemId)))  ){
                    myflag = "unValidEntery";
                    alert ("Unable To Add To Cart : All The Available Items In Stock Where Added To Your Cart : ");
                }

            });
            
        }

        

       
        if ( myflag == "updated" ){              //....Item already in cart Thus increase Qty

            var updatedProduct = new Object();    
                updatedProduct[itemId] = qty; 
                myCartCollection.splice(itemOrder,1,updatedProduct);  

                allCartInfo = {
                    myCartCollection: myCartCollection,
                    totalQty:totalQty + 1,
                    totalPrice: Math.ceil(totalPrice + products[itrInProducts].Price)};

        }else if(myflag == "new"){          //..........Item not in cart add item
            var newProduct = new Object();
                newProduct[itemId] = 1; 
                myCartCollection.push(newProduct);

                allCartInfo = {
                    myCartCollection: myCartCollection,
                    totalQty:totalQty + 1,
                    totalPrice: Math.ceil(totalPrice + products[itrInProducts].Price)};
        }
   
    
    
    }else{
        var newProduct = new Object();
            newProduct[itemId] = 1; 
        var myCartCollection = new Array();
            myCartCollection.push(newProduct);

        allCartInfo = {
            myCartCollection:myCartCollection,
            totalQty:1,
            totalPrice: Math.ceil(products[itrInProducts].Price)
        };

    }

    
    
    localStorage.setItem("allCartInfo",JSON.stringify(allCartInfo));
}
/*
.
. . . . . display check out
. 
*/
function displayCheckOutDiv(itemId,flag) {

    if (flag == true){
        updateCart(itemId);
    }
    
    var oldCheckOut = document.getElementById("chOut");
        if (oldCheckOut) 
        {
            body.removeChild(oldCheckOut);
            
        }


    
    if (   
        (flag == true) || 
        (flag == false && JSON.parse(localStorage.getItem("allCartInfo")))   
    ){
 
        var allCartInfo = JSON.parse(localStorage.getItem("allCartInfo"));

        var checkOut = document.createElement("div");   
            checkOut.setAttribute("class","checkOut");
            checkOut.setAttribute("id","chOut");


        var number_Div = document.createElement("div");
            number_Div.setAttribute("class","number_Div");
            number_Div.innerHTML = allCartInfo["totalQty"];
            checkOut.appendChild(number_Div);   


        var productPrice_checkOut = document.createElement("label");
            productPrice_checkOut.setAttribute("class","productPrice_checkOut");
            productPrice_checkOut.innerHTML = "$"+allCartInfo["totalPrice"];
            checkOut.appendChild(productPrice_checkOut);

        var img_checkOut = document.createElement("img");    
            img_checkOut.setAttribute("class","img_checkOut");
            img_checkOut.setAttribute("src","Assets/cart.png");
            checkOut.appendChild(img_checkOut);

        var checkOut_word = document.createElement("label");   
            checkOut_word.setAttribute("class","checkOut_word");
            checkOut_word.innerHTML = "Checkout";
            checkOut_word.addEventListener("click",function(){
                clearPageContent();
                stylePage("Style_css/cartPage.css");
                loadPage("Pages_js/cartPage.js");
            });
            checkOut.appendChild(checkOut_word); 
            body.appendChild(checkOut);       

    }
            
   
 
}
/*
.
. Function isValid when qty in cart excced quantity of stock produce alart and
.
*/
function isAccepted(addedItemId) {

    var inStockQty = 0;
    var valueToReturn ;
    if(JSON.parse(localStorage.getItem("allCartInfo"))){
        allCartInfo = JSON.parse(localStorage.getItem("allCartInfo"));
        myCartCollection = allCartInfo["myCartCollection"];

        for (var itr in products)
        {

            Object.entries(products[itr]).forEach(function([key,value]) {  

                if(products[itr].ProductId == addedItemId)        
                {
                    inStockQty = products[itr].Quantity; 
                }

            });
            
        }

        for (var itr in myCartCollection)
        {

            Object.entries(myCartCollection[itr]).forEach(function([key,value]) {  
                if( key == addedItemId )        
                {
                    if(value == inStockQty){

                        valueToReturn = false;
                    }else{

                        valueToReturn = true;  
                    }
                }

            });
            
        }
        //console.log(valueToReturn);
        return valueToReturn ;
    }
     
}
/* .
.
.function inputUserqty takes qty from user and input to cart 
.
 */

function inputUserQty(requiredQty,requiredItemId,totalQty) {

    var idsInCart = new Array();
    var totalQty ; 
    var qtyInCart;
    var remainingQty_ToBeAdd;
    var count;
    
    //...... If There Was A cart Check if the required item in it
    if (JSON.parse(localStorage.getItem("allCartInfo"))){               

        var allCartInfo = JSON.parse(localStorage.getItem("allCartInfo"));
        var myCartCollection = allCartInfo["myCartCollection"];
        
        for (var t in myCartCollection)
        {
            Object.entries(myCartCollection[t]).forEach(function([key,value]) {   //.. keep ids of product in cart at an array
                idsInCart.push(key);
            });
        }   


   //___________________________________________________________________________________________//
        
        if (  idsInCart.includes(requiredItemId) ){     //.. if The Product Was In Cart Calculate The Qty In The Cart
            for (var x in myCartCollection)
            {
                Object.entries(myCartCollection[x]).forEach(function([key,value]) { 
                    if(key == requiredItemId){
                        qtyInCart = value;
                    }
                });
            }   
            remainingQty_ToBeAdd = totalQty - qtyInCart ;   //..Calculate The Available Qty In Stock
            
            
        }else{                        //..If The Product Is Not In Cart The Available Qty = Total Qty from the server
            remainingQty_ToBeAdd = totalQty - 0 ;
        } 
        
        if(requiredQty <= remainingQty_ToBeAdd){

            for(count = 1 ; count <= requiredQty ; count++){ 
                displayCheckOutDiv(requiredItemId,true); 
            }
        }else{
            var msg = "The Remaining Quantity In stock is : "+remainingQty_ToBeAdd;
            alert(msg);
        }   
              
    }else{  // .............if there is no cart than add directly
        
        remainingQty_ToBeAdd = totalQty - 0 ;
        if(requiredQty <= remainingQty_ToBeAdd){

            for(count = 1 ; count <= requiredQty ; count++){ 
                displayCheckOutDiv(requiredItemId,true); 
            }
        }else{
            var msg = "The Remaining Quantity In stock is : "+remainingQty_ToBeAdd;
            alert(msg);
        }   
            
    }  
       
}
    

