//................Retrive body and remove all children..............


createHeader();

var mainDiv = document.createElement("main");
    mainDiv.setAttribute("class","mainDiv");
    mainDiv.setAttribute("id","mainDiv");    
 

//Retrive Cart Data from Local Storage...................

var allCartInfo = JSON.parse(localStorage.getItem("allCartInfo"));
var myCartCollection = allCartInfo["myCartCollection"];
var products_inCart_ids = [];
var products_inCart_qtys = [];
var totalPrice = 0;
var totalQty = 0; 

//Dispaly Page Contents 
// 1) TiTle Div

var titleDiv = document.createElement("div");
    titleDiv.setAttribute("class","titleDiv");
    titleDiv.innerHTML = "Cart";

    mainDiv.appendChild(titleDiv);
    
// 2) Notation Bar

var notationDiv = document.createElement("div");
    notationDiv.setAttribute("class","notationDiv");
    
var labelProduct = document.createElement("lable"); 
    labelProduct.setAttribute("class","labelProduct");
    labelProduct.innerHTML = "Product";

var labelPrice = document.createElement("lable");
    labelPrice.setAttribute("class","labelPrice");
    labelPrice .innerHTML = "Price";

var labelQuantity = document.createElement("lable"); 
    labelQuantity.setAttribute("class","labelQuantity");
    labelQuantity.innerHTML = "Quantity";

var labelTotal = document.createElement("lable");
    labelTotal.setAttribute("class","labelTotal");
    labelTotal .innerHTML = "Total";

var lineNotation = document.createElement("div");
    lineNotation.setAttribute("class","lineNotation");

    notationDiv.appendChild(labelProduct);
    notationDiv.appendChild(labelPrice);
    notationDiv.appendChild(labelQuantity);
    notationDiv.appendChild(labelTotal);         //Qty*Price
    notationDiv.appendChild(lineNotation);
    mainDiv.appendChild(notationDiv);

// 3) Each Product Info

for (var itr in myCartCollection) 
{
       Object.entries(myCartCollection[itr]).forEach(function([key,value]) {
         
            products_inCart_ids.push(key);
            products_inCart_qtys.push(value); //-->

    });
}


for (var itr in products) 
{
        
    if( products_inCart_ids.includes(products[itr].ProductId,0) ) {
            
        var eachDiv = document.createElement("div");
            eachDiv.setAttribute("class","eachDiv");

        var imgDiv = document.createElement("div");             //...........From all products DataBase
            imgDiv.setAttribute("class","imgDiv");
        
        var nameDiv = document.createElement("div");             //...........From all products DataBase           
            nameDiv.setAttribute("class","nameDiv");  
            
        var priceDiv = document.createElement("div");            //...........From all products DataBase
            priceDiv.setAttribute("class","priceDiv");  
        
        var quantityDiv = document.createElement("div");         //...........From cart products DataBase
            quantityDiv.setAttribute("class","quantityDiv");  
            
        var totalDiv = document.createElement("div");           //............Calculated
            totalDiv.setAttribute("class","totalDiv");  
                
        Object.entries(products[itr]).forEach(function([key,value]) {

            switch(key) {

                    case "ProductPicUrl":
                    var ProductPicUrl = document.createElement("img");
                        ProductPicUrl.setAttribute("class","ProductPicUrl");
                        ProductPicUrl.setAttribute("src",value);
                    imgDiv.appendChild(ProductPicUrl);
                    break;
                    
                    
                    case "Name":
                    var productName = document.createElement("label");
                        productName.setAttribute("class","productName");
                        productName.innerHTML = value;
                    nameDiv.appendChild(productName);
                    break;

                    case "Quantity":

                    var myProductId = products[itr].ProductId;
                    for (var j in myCartCollection) 
                        {
                            Object.entries(myCartCollection[j]).forEach(function([key,value]) {
                                if (key == myProductId) {
                                    quantityDiv.innerHTML =  value;
                                }
                            });
                        }  
                    break; 
                    
                    case "Price":
                    var productPrice = document.createElement("label");
                        productPrice.setAttribute("class","productPrice");
                        productPrice.innerHTML = "$"+value;
                    priceDiv.appendChild(productPrice);
                    //-----------Calculate Price for each product = qty * price

                    var index = products_inCart_ids.indexOf(products[itr].ProductId);             
                    var QtyPerProduct = products_inCart_qtys[index];
                    var totalPricePerProduct = QtyPerProduct * value ;
                        totalDiv.innerHTML = "$"+totalPricePerProduct;
                        totalPrice = totalPrice + totalPricePerProduct;  //-->
                            
                    break; 
                } 
            });
            eachDiv.appendChild(imgDiv);
            eachDiv.appendChild(nameDiv);
            eachDiv.appendChild(priceDiv);
            eachDiv.appendChild(quantityDiv);
            eachDiv.appendChild(totalDiv);
            mainDiv.appendChild(eachDiv);
                
    }       
       
       
}


//4) cart Totals

var totalsDiv = document.createElement("div");
    totalsDiv.setAttribute("class","totalsDiv");

var labelCart = document.createElement("div");
    labelCart.setAttribute("class","labelCart");
    labelCart.innerHTML = "Cart Totals";
    
var line = document.createElement("div");
    line.setAttribute("class","line");

var lineColored = document.createElement("div");
    lineColored.setAttribute("class","lineColored");
    
var labelSum = document.createElement("div");
    labelSum.setAttribute("class","labelSum");
    labelSum.innerHTML = "Total";

var sum = document.createElement("div");
    sum.setAttribute("class","sum");
    sum.innerHTML = "$"+totalPrice;

    totalsDiv.appendChild(labelCart);
    totalsDiv.appendChild(line);
    totalsDiv.appendChild(lineColored);
    totalsDiv.appendChild(labelSum);
    totalsDiv.appendChild(sum);
    mainDiv.appendChild(totalsDiv);


body.appendChild(mainDiv);
