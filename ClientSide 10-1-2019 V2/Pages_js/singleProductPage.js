//................Retrive body and remove all children.............

createHeader();

var mainDiv = document.createElement("main");
    mainDiv.setAttribute("class","mainDiv");  
    mainDiv.setAttribute("id","mainDiv");    
  

//Retrive Products Data from Local Storage...................

var clickedProductOrder;                                             //.....Address of chosen product in allProducts array


//..............Find itr or (the order in array) of chosen productname
for (var itr in products) {

    Object.entries(products[itr]).forEach(function([key,value]) {

        switch(key) {             
            case "ProductId":
            if (chosenItemId == value)
            {
                clickedProductOrder = itr;
            }
            break;
        }   
    });
}

//..............Load Data Of Single Product

var imgDiv = document.createElement("div");
    imgDiv.setAttribute("class","imgDiv");

var categoryDiv = document.createElement("div");
    categoryDiv.setAttribute("class","categoryDiv");
var Category_Word = document.createElement("label");
    Category_Word.setAttribute("class","Category_Word");
    Category_Word.innerHTML = products[clickedProductOrder].Category;
    categoryDiv.appendChild(Category_Word);

var avilabilityDiv = document.createElement("div");
    avilabilityDiv.setAttribute("class","avilabilityDiv");


Object.entries(products[clickedProductOrder]).forEach(function([key,value]) {

    switch(key) {
        
        case "ProductPicUrl":
        var ProductPicUrl = document.createElement("img");
            ProductPicUrl.setAttribute("class","ProductPicUrl");
            ProductPicUrl.setAttribute("src",value);
            imgDiv.appendChild(ProductPicUrl);
        break;
        
        
        case "Name":
        var productName = document.createElement("label");
            productName.setAttribute("class","productName");
            productName.innerHTML = value;
            categoryDiv.appendChild(productName);
        break;
        case "Description":
        var productDescription = document.createElement("label");
            productDescription.setAttribute("class","productDescription");
            productDescription.innerHTML = value;  
            categoryDiv.appendChild(productDescription);
        break;


        case "Quantity":
        var productStatus = document.createElement("label");
            productStatus.setAttribute("class","productStatus");
            productStatus.innerHTML = "Availability:";
            avilabilityDiv.appendChild(productStatus);

            if (value > 0) {
                var instock_word = document.createElement("label");
                    instock_word.setAttribute("class","instock_word");
                    instock_word.innerHTML = "In stock";
                    avilabilityDiv.appendChild(instock_word);
                
                var line = document.createElement("div");
                    line.setAttribute("class","line");
                    avilabilityDiv.appendChild(line);   
            }
            else {
                var outstock_word = document.createElement("label");
                    outstock_word.setAttribute("class","outstock_word");
                    outstock_word.innerHTML = "Out of stock";
                    avilabilityDiv.appendChild(outstock_word);

                var line = document.createElement("div");
                    line.setAttribute("class","line");
                    avilabilityDiv.appendChild(line);      
            }
        var Quantity_Word = document.createElement("label");
            Quantity_Word.setAttribute("class","Quantity_Word");
            Quantity_Word.innerHTML = "Quantity:";
            avilabilityDiv.appendChild(Quantity_Word); 
        // Quantity Value
        var QuantityDiv = document.createElement("input");
            QuantityDiv.setAttribute("type","text");
            QuantityDiv.setAttribute("class","QuantityDiv");
            QuantityDiv.setAttribute("value","1");
            avilabilityDiv.appendChild(QuantityDiv);    
        // ADD TO CART BUTTON
        var addToCartButton =  document.createElement("div");
            addToCartButton.setAttribute("class","addToCartButton");
            addToCartButton.setAttribute("id",products[clickedProductOrder].ProductId);
            addToCartButton.innerHTML = "Add to cart";
            addToCartButton.addEventListener("click",function(){ 

                if (/^[0-9]+$/.test(QuantityDiv.value)) {

                    inputUserQty(QuantityDiv.value,this.id,products[clickedProductOrder].Quantity);
                }else{
                    alert("Please Enter Only Numbers");
                }
            });   
        
        var cartIcon = document.createElement("img");    
            cartIcon.setAttribute("class","cartIcon");
            cartIcon.setAttribute("src","Assets/cart.png");
            addToCartButton.appendChild(cartIcon);
            avilabilityDiv.appendChild(addToCartButton);        
        break; 
        
        case "Price":
        var productPrice = document.createElement("label");
            productPrice.setAttribute("class","productPrice");
            productPrice.innerHTML = "$"+value;
            avilabilityDiv.appendChild(productPrice);
        break;
        


    } 

    mainDiv.appendChild(imgDiv);
    mainDiv.appendChild(categoryDiv);
    mainDiv.appendChild(avilabilityDiv);
    body.appendChild(mainDiv);   
});

displayCheckOutDiv(0,false);

/* 
[{"ProductId":"HT-1000","Category":"Laptops","MainCategory":"Computer Systems",
"TaxTarifCode":"1","SupplierName":"Very Best Screens","WeightMeasure":4.2,"WeightUnit":"KG",
"Description":"Notebook Basic 15 with 2,80 GHz quad core, 15\" LCD, 4 GB DDR3 RAM, 500 GB Hard Disc,
Windows 8 Pro","Name":"Notebook Basic 15","DateOfSale":"2017-03-26",
"ProductPicUrl":"https://openui5.hana.ondemand.com/test-resources/sap/ui/documentation/sdk/images/HT-1000.jpg",
"Status":"Available","Quantity":10,"UoM":"PC","CurrencyCode":"EUR","Price":956,"Width":30,"Depth":18,
"Height":3,"DimUnit":"cm"},
*/


