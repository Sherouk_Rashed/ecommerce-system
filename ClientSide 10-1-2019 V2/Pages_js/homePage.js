//Main Intro Interface for the webSite..............

   
createHeader();

var mainDiv = document.createElement("div");
    mainDiv.setAttribute("class","mainDiv"); 
    mainDiv.setAttribute("id","mainDiv");    
   


//Retrive Home Data from the server Using "GET"...................


var serverReply = '';
var httpReq = new XMLHttpRequest();
httpReq.open("GET", "https://gist.githubusercontent.com/a7med-hussien/7fc3e1cba6abf92460d69c0437ce8460/raw/da46abcedf99a3d2bef93a322641926ff60db3c3/products.json",true);

httpReq.onreadystatechange = function() {
       
    if(this.readyState === 4 && this.status === 200) {

        if (typeof(this.responseText) == "string") {
            serverReply = JSON.parse(this.responseText);
        }
        else {
            serverReply = this.responseText;
        }
        
        //THE SERVER RESPONDED.....  Object { ProductCollection: (123) […], ProductCollectionStats: {…} }               

        products = serverReply["ProductCollection"]; 
        //console.log(products);
        //Display products on homePage........................
        for(var i in products){

            var secDiv = document.createElement("div");
                secDiv.setAttribute("class","secDiv");
                secDiv.setAttribute("id",products[i].ProductId);
                secDiv.addEventListener("click",function() //-----------function to go to other page   
                       {                        
                        chosenItemId = this.id;            //---Global var
                        clearPageContent();
                        stylePage("Style_css/singleProductPage.css");
                        loadPage("Pages_js/singleProductPage.js");
                       });
                

            var singleProduct = products[i];
            Object.entries(singleProduct).forEach(function([key,value]) {
                
                switch(key) {
                    
                    case "Name":
                    var productName = document.createElement("label");
                        productName.setAttribute("class","productName");
                        productName.innerHTML = value;
                        secDiv.appendChild(productName);
                    break;

                    case "Price":
                    var productPrice = document.createElement("label");
                        productPrice.setAttribute("class","productPrice");
                        productPrice.innerHTML = "$"+value;
                        secDiv.appendChild(productPrice);
                    break;

                    case "ProductPicUrl":
                    var ProductPicUrl = document.createElement("img");
                        ProductPicUrl.setAttribute("class","ProductPicUrl");
                        ProductPicUrl.setAttribute("src",value);
                        secDiv.appendChild(ProductPicUrl);
                    break; 

                }
               
            });
            var cartIcon = document.createElement("img");    //.............. add Event Listener to the cart icon
                cartIcon.setAttribute("class","cartIcon");
                cartIcon.setAttribute("src","Assets/cart.png");
                cartIcon.addEventListener("click",function(event){

                    displayCheckOutDiv(this.parentElement.id,true)
                    event.stopPropagation();     
                });
            
                secDiv.appendChild(cartIcon);     
                mainDiv.appendChild(secDiv);
        
        }
    body.appendChild(mainDiv);
    }
};

httpReq.send();
/* 
.
.............................................checkOutDiv................................................
. 
*/

displayCheckOutDiv(0,false);  


//--------ceckOut div--------- 
//--case 1 : No div and user pressed the cart : 1 update cart / 2 create div
//--case 2 : div exists and user pressed the cart : 1 update cart / 2 display div
//--case 3 : div load when home called (if there is items in  cart) :1) don't update cart  / 2 display div
//--case 4 : div load when home called (no items in  cart) :1) do nothing 
//(false)-> don't update cart



